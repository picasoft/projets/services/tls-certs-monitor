# TLS Certs Monitor

## Explication

[TLS Certs Monitor](https://gitlab.utc.fr/picasoft/projets/tls-cert-monitor) est un outil permettant de faciliter la génération de certificats pour les services non-HTTPS.

En effet, notre reverse-proxy Traefik génère automatiquement des certificats pour chaque service indiquant le bon label dans le Docker Compose. En revanche, dans la version que nous utilisons, ce n'est qu'un reverse-proxy HTTP/HTTPS (web), il ne peut pas servir de frontend pour d'autres services TCP, comme le serveur LDAP, le serveur mail, une base de donnée, etc.

En revanche, sa facilité à générer des certificats et surtout le désir de centralisation de la gestion des certificats a poussé le développement de cet outil, qui évite de mettre en place un `certbot` par service, par exemple.

Basiquement, il va extraire les certificats générés par Traefik et les mettre à disposition des services.

La documentation complète est disponible [ici](https://gitlab.utc.fr/picasoft/projets/tls-cert-monitor)

## Déploiement

Il n'y a pas de configuration spécifique à ce service ; la configuration se fait sur les services supervisés. Il suffit de déployer ce conteneur avec un `docker-compose up -d` sur tous les hôtes souhaités (*i.e.* qui contiennent des services non-HTTPS qui ont besoin de certificats ; `monitoring` est le premier candidat avec le serveur mail et le serveur LDAP).

## Sécurité

Bien évidemment, les clés privées générées sont **extrêmement sensibles**. C'est pourquoi les fichiers générés ne sont **pas** enregistrés dans un volume géré par Docker Compose. En effet, supposons que tous les certificats soient générés dans un volume `certs`.

Tout service voulant récupérer son certificat devrait monter `certs`, ayant ainsi accès aux clés privées des autres services, ce qui poserait problème en cas de compromission du conteneur.

On pourrait alors envisager de créer un volume par service, comme `ldap_certs`, `mail_certs`... Mais ceci impliquerait de modifier le présent [docker-compose.yml](./docker-compose.yml) à chaque fois, alors qu'il est autonome avec le système actuel.

On a choisi une bonne fois pour toutes `/DATA/docker/certs` comme dossier de stockage des certificats, et par convention, les services monteront `/DATA/docker/certs/<domain>` pour ne pas risquer de compromettre d'autres services.

## Mise à jour

Il suffit de mettre à jour la variable d'environnement `TLS_CERTS_MONITOR_VERSION` dans le Dockerfile, quand une nouvelle release (tag) apparaît sur le dépôt, ainsi que le numéro de version de l'image dans Compose.
